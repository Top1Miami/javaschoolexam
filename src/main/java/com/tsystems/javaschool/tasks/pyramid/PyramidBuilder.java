package com.tsystems.javaschool.tasks.pyramid;

import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        if(inputNumbers.contains(null) || inputNumbers.size() == 2) {
            throw new CannotBuildPyramidException();
        }
        int pyramidSize = (int) (-1 + (long) Math.sqrt((double) (1 + 8 * (long) inputNumbers.size()))) / 2;
        if (pyramidSize * pyramidSize + pyramidSize - 2 * inputNumbers.size() == 0) {
            inputNumbers.sort(Comparator.naturalOrder());
            int lineSize = 2 * pyramidSize - 1;
            int[][] pyramid = new int[pyramidSize][lineSize];
            int k = 0;
            for(int i = 0; i < pyramidSize;i++) {
                for(int j = pyramidSize - i - 1; j < lineSize - (pyramidSize - i - 1);j+=2, k++) {
                    pyramid[i][j] = inputNumbers.get(k);
                }
            }
//            for(int i = 0; i < pyramidSize;i++) {
//                for(int j = 0;j < lineSize;j++) {
//                    System.out.print(pyramid[i][j]);
//                }
//                System.out.println();
//            }
            return pyramid;
        } else {
            throw new CannotBuildPyramidException();
        }
    }


}
