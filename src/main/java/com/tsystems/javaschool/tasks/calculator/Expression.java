package com.tsystems.javaschool.tasks.calculator;

/**
 * Created by Dima on 04.06.2019.
 */
public interface Expression {
    double evaluate() throws ArithmeticException;
}
