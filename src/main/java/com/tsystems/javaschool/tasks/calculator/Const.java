package com.tsystems.javaschool.tasks.calculator;

/**
 * Created by Dima on 04.06.2019.
 */
public class Const implements Expression {
    private double value;

    public Const(double param) {
        value = param;
    }

    @Override
    public double evaluate() throws ArithmeticException {
        return value;
    }

    @Override
    public String toString() {
        return "(" + String.valueOf(value) +")";
    }

}