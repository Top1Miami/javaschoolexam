package com.tsystems.javaschool.tasks.calculator;

/**
 * Created by Dima on 04.06.2019.
 */
public class Divide implements Expression{
    private Expression exp1;
    private Expression exp2;

    public Divide(Expression exp1, Expression exp2) {
        this.exp1 = exp1;
        this.exp2 = exp2;
    }

    @Override
    public double evaluate() throws ArithmeticException {
        double val = exp2.evaluate();
        if(val == 0) {
            throw new ArithmeticException();
        }
        return exp1.evaluate() / exp2.evaluate();
    }

    @Override
    public String toString() {
        return "(" + exp1.toString() + "/" + exp2.toString() + ")";
    }

}
