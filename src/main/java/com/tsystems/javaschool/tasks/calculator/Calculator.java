package com.tsystems.javaschool.tasks.calculator;

public class Calculator {

    private int pos;
    private String statement;
    private Token curToken;
    private double numValue;
    private boolean exception;

    private enum Token {
        NUM,
        PLUS,
        MINUS,
        MUL,
        DIV,
        LP,
        RP,
        UNMIN,
        END
    }

    private void init(String statement) {
        exception = false;
        pos = 0;
        this.statement = statement;
        curToken = null;
        numValue = 0;
    }

    private void skipBraces() {
        while (pos < statement.length() && Character.isWhitespace(statement.charAt(pos))) {
            pos++;
        }
    }

    private char get() {
        if (pos < statement.length()) {
            return statement.charAt(pos);
        }
        return '\0';
    }

    private char getAndMove() {
        char c = get();
        pos++;
        return c;
    }

    private Token getToken() {
        skipBraces();
        char c = getAndMove();
        if (Character.isDigit(c)) {
            return parseNumber();
        } else if (c == '+') {
            return curToken = Token.PLUS;
        } else if (c == '-') {
            if (curToken == Token.RP || curToken == Token.NUM) {
                return curToken = Token.MINUS;
            } else {
                curToken = Token.UNMIN;
            }
        } else if (c == '*') {
            return curToken = Token.MUL;
        } else if (c == '/') {
            return curToken = Token.DIV;
        } else if (c == '(') {
            return curToken = Token.LP;
        } else if (c == ')') {
            return curToken = Token.RP;
        } else if (c == '\0') {
            return curToken = Token.END;
        }
        exception = true;
        return null;
    }

    private Token parseNumber() {
        int start = pos;
        while (Character.isDigit(get())) {
            getAndMove();
        }
        if (get() == '.') {
            getAndMove();
        }
        while (Character.isDigit(get())) {
            getAndMove();
        }
        String number = "";
        if (curToken == Token.UNMIN) {
            number = "-";
        }
        number += statement.substring(start - 1, pos);
        numValue = Double.valueOf(number);
        return curToken = Token.NUM;
    }

    private Expression primitive() {
        getToken();
        if (curToken == Token.NUM) {
            Expression number = new Const(numValue);
            getToken();
            return number;
        } else if (curToken == Token.UNMIN) {
            return new UnaryMinus(primitive());
        } else if (curToken == Token.LP) {
            Expression expression = plusMinus();
            getToken();
            return expression;
        }
        exception = true;
        return null;
    }

    private Expression multiplyDivide() {
        Expression e1 = primitive();
        while (curToken == Token.MUL || curToken == Token.DIV) {
            if (curToken == Token.MUL) {
                e1 = new Multiply(e1, primitive());
            } else {
                e1 = new Divide(e1, primitive());
            }
        }
        return e1;
    }

    private Expression plusMinus() {
        Expression e1 = multiplyDivide();
        while (curToken == Token.PLUS || curToken == Token.MINUS) {
            if (curToken == Token.MINUS) {
                e1 = new Minus(e1, multiplyDivide());
            } else {
                e1 = new Add(e1, multiplyDivide());
            }
        }
        return e1;
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null) {
            return null;
        }
        init(statement);
        Expression res = plusMinus();
        if (pos != statement.length() + 1) {
            return null;
        }
        if (exception) {
            return null;
        } else {
            try {
                String result = String.valueOf(res.evaluate());
                if (result.endsWith(".0")) {
                    return result.substring(0, result.length() - 2);
                } else {
                    if (result.length() - result.lastIndexOf((int) '.') - 1 > 4) {
                        return result.substring(0, result.lastIndexOf((int) '.') + 4);
                    }
                    return result;
                }
            } catch (ArithmeticException e) {
                return null;
            }
        }
    }

}
