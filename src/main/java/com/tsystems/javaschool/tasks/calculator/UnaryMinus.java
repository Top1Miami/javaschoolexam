package com.tsystems.javaschool.tasks.calculator;

/**
 * Created by Dima on 04.06.2019.
 */
public class UnaryMinus implements Expression {
    Expression exp1;

    public UnaryMinus(Expression exp1) {
        this.exp1 = exp1;
    }

    @Override
    public double evaluate() throws ArithmeticException {
        return -exp1.evaluate();
    }

    @Override
    public String toString() {
        return "( -" + exp1.toString() + ")";
    }

}
